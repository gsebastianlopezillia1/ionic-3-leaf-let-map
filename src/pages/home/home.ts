import { Component, ViewChild, ElementRef } from '@angular/core';
import { NavController } from 'ionic-angular';
import leaflet from 'leaflet';//http://tphangout.com/ionic-3-leaflet-maps-geolocation-markers/

import { Geolocation } from '@ionic-native/geolocation';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  @ViewChild('map') mapContainer: ElementRef;
  /**
   * reference to the map object
   */
  map: any

  /**
   * default tiles url
   */
  tyle: any = 'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png'

  /**
   * set of urls availables
   */
  tilesURLs: any = [
    { 'desc': 'Satellite', 'url': 'https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}' },//'http://{s}.google.com/vt/lyrs=m&x={x}&y={y}&z={z}' },
    { 'desc': 'Terrain', 'url': 'https://server.arcgisonline.com/ArcGIS/rest/services/World_Terrain_Base/MapServer/tile/{z}/{y}/{x}' },
    { 'desc': 'Streets', 'url': 'https://server.arcgisonline.com/ArcGIS/rest/services/World_Street_Map/MapServer/tile/{z}/{y}/{x}' },
    { 'desc': 'Standar', 'url': 'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png' },
    { 'desc': 'Hike&Byke', 'url': 'http://{s}.tiles.wmflabs.org/hikebike/{z}/{x}/{y}.png' }
  ]

  GPSoptions: any = { timeout: 30000, enableHighAccuracy: true, maximumAge: 3600 };

  constructor(public navCtrl: NavController,
    public geolocation: Geolocation) {

  }

  ionViewDidEnter() {
    this.drawMap(this.tyle)
  }

  centerMe() {
    this.map.locate({
      enableHighAccuracy: true,
      maximumAge: 36000,
      setView: true,
      maxZoom: 13
    })
  }

  selectTyle() {
    this.drawMap(this.tyle)
  }

  drawMap(tileUrl) {
    document.getElementById('mapContainer').innerHTML = '<div id="map" style="width:100%; height:100%;"></div>';
    this.map = leaflet.map("map").fitWorld();
    this.geolocation.getCurrentPosition(this.GPSoptions).then((position) => {
      leaflet.tileLayer(tileUrl, {
        maxZoom: 20
      }).addTo(this.map);
      let markerGroup = leaflet.featureGroup();
      let marker: any = leaflet.marker([position.coords.latitude, position.coords.longitude]).on('click', () => {
        alert('Marker clicked');
      })
      markerGroup.addLayer(marker);
      this.map.addLayer(markerGroup)
      this.centerMe()
    }, (error) => {
      console.log('Error getting location', error);
    });


  }

}
